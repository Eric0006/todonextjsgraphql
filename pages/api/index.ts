import {ApolloServer}   from "apollo-server-micro";
import {intArg, makeSchema, nonNull, objectType, stringArg} from "nexus";
import path from "path";
import prisma from '../../libs/context'


const Todo = objectType({
    name:"Todo",
    definition(t){
    t.id('id')
    t.string('title')
   t.nullable.string('content')
    }
})

const Query = objectType({
    name:'Query',
    definition(t){
    t.list.field('todos',{
        type:"Todo",
        resolve:()=>{
            return prisma.todo.findMany({})
        }
    })

    t.field('todo',{
        type:"Todo",
        args:{
            todoId:nonNull(intArg())
        },

      resolve:(parent,args)=>{
          return prisma.todo.findUnique({
          where:{id:args.todoId}

          })
      }
    })



    }
})

const Mutations = objectType({
    name:'Mutation',
    definition(t){
        t.field("createTodo", {
            type:"Todo",
            args:{
                title:nonNull(stringArg()),
                content: stringArg()
            },
            resolve:(_,{title,content})=>{
                return prisma.todo.create({
                    data:{
                        title,
                        content
                    }
                })
            }
        })

        t.field("deleteTodo", {
            type:"Todo",
            args:{
                todoId:nonNull(intArg())
            },

            resolve:(_, {todoId})=>{
                return prisma.todo.delete({
                    where:{
                        id:todoId
                    }
                })
            }
        })



        t.field("updateTodo", {
            type:"Todo",
            args:{
                todoId:nonNull(intArg()),
                title:nonNull(stringArg()),
                content: stringArg()
            },

            resolve:(_, args)=>{
                return prisma.todo.update({
                    where:{
                        id:args.todoId
                    },
                    data:{
                        title:args.title,
                        content:args.content
                    }
                })
            }
        })

    }
})



export const schema = makeSchema({
    types: [Todo,Query, Mutations],
    outputs: {
      typegen: path.join(process.cwd(), 'pages/api/nexus-typegen.ts'),
      schema: path.join(process.cwd(), 'pages/api/schema.graphql'),
    },
  })
  
  export const config = {
    api: {
      bodyParser: false,
    },
  }
  
 // /home/eric/Desktop/nextjs_projects/todographql/pages/api/nexus-typegen.ts


export default new ApolloServer({ schema }).createHandler({
    path: '/api',
  })